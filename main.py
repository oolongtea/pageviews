from collections import defaultdict
from datetime import datetime
import json
import logging
from pathlib import Path

from utility import (each_hour_in_range,
        each_line_of_filepath,
        get_and_write_file)
# since the repo is public, protect some things in an uncommited file
from secrets import BLACKLIST_URL, BASE_URL


def get_blacklisted_articles(filepath):
    blacklist = list()
    for line in each_line_of_filepath(filepath):
        blacklist.append(tuple(line.split(' ')))
    return set(blacklist)


def compute_top_articles(filepath, blacklist=None):
    if blacklist is None:
        blacklist = set()

    results_filepath = filepath.parent / (filepath.parts[-1] + '.results')
    if results_filepath.exists():
        with open(results_filepath, 'rb') as fb:
            top_articles_by_code = json.load(fb)
        return results_filepath

    codes = defaultdict(list)
    for line in each_line_of_filepath(filepath):
        domain_code, page_title, count_views, _ = line.split()
        if (domain_code, page_title) in blacklist:
            continue
        codes[domain_code].append((page_title, int(count_views)))

    top_articles_by_code = dict()
    for code in codes.keys():
        top_25 = sorted(codes[code], key=lambda x: x[1], reverse=True)[:25]
        top_articles_by_code[code] = [x[0] for x in top_25]

    with open(results_filepath, 'w') as f:
        json.dump(top_articles_by_code, f)

    return results_filepath


def compute_wikipedia_top_25_articles(evaluation_hour, blacklist=None):
    url = BASE_URL.format(evaluation_hour)
    filepath = get_and_write_file(url)
    return compute_top_articles(filepath, blacklist=blacklist)


def get_result_files_for_pageviews(from_datetime, to_datetime):
    if from_datetime < datetime(2015, 5, 1, 1):
        raise ValueError("There is no data before 2015-05-01 01:00")

    filepath_blacklist = get_and_write_file(BLACKLIST_URL,
                                            to_filepath=Path('blacklist_domains_and_pages'))
    blacklist = get_blacklisted_articles(filepath_blacklist)

    results_filepaths = list()
    for evaluation_hour in each_hour_in_range(from_datetime, to_datetime):
        logging.info(evaluation_hour)
        results_filepath = compute_wikipedia_top_25_articles(evaluation_hour, blacklist)
        results_filepaths.append(results_filepath)
        logging.info(results_filepath)

    return results_filepaths
