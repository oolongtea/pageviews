from argparse import ArgumentParser, ArgumentTypeError
from datetime import datetime
import logging
import sys

from main import get_result_files_for_pageviews


DATE_FORMAT = '%Y-%m-%d %H'


def parse_argument_datetimes(start_time, end_time):
    if end_time is None:
        end_time = datetime.now()
    else:
        end_time = datetime.strptime(end_time, DATE_FORMAT)

    if start_time is None:
        start_time = end_time
    else:
        start_time = datetime.strptime(start_time, DATE_FORMAT)

    return start_time, end_time


def validate_datetime(string):
    try:
        datetime.strptime(string, DATE_FORMAT)
    except Exception:
        raise ArgumentTypeError("%s is not a valid date format", string)

    return string


def add_arguments(parser):
    parser.add_argument('-s', '--start-time', action='store',
                        default=None, type=validate_datetime,
                        help="Date format example: '2019-10-01 06'. "
                             "Can be omitted to default to --end-time")
    parser.add_argument('-e', '--end-time', action='store',
                        default=None, type=validate_datetime,
                        help="Date format example: '2019-10-01 06'. "
                             "Default is timestamp of now.")


def main(argv=None):
    """
    317MB for date=2019-10-01, hour=4, starting from having to download raw file
    64KB for same date/hour, with results cached
    """
    if argv is None:
        argv = sys.argv

    parser = ArgumentParser(description="Serialize top 25 pages for each "
                                        "Wikipedia sub-domain for a given hour "
                                        "or range by total pageviews to a "
                                        "local results file.")
    add_arguments(parser)

    options = parser.parse_args(argv[1:])
    start_time, end_time = parse_argument_datetimes(options.start_time, options.end_time)

    try:
        results_filepaths = get_result_files_for_pageviews(start_time, end_time)
        logging.info(results_filepaths)
    except Exception:
        logging.exception("Something went wrong when trying to compute pageviews")
        return 1

    return 0


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    sys.exit(main())
