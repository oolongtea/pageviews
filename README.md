# setup

- update the blacklist URL in secrets.py
- `python3 -m venv pageviews`
- `source pageviews/bin/activate`
- `pip install -r requirements-dev.txt`
- `python run.py` or `python run.py --start-time '2019-10-01 00' --end-time '2019-10-01 05'`

# workflow overview


this is an overview of `get_result_files_for_pageviews` in main.py

- validate input date
- fetch blacklist file, cache it locally, and build a set of (domain, article)
- compute top 25 articles per hour in range

this is an overview of `compute_wikipedia_top_25_articles`

- check if results file is available, return that filepath if we're good
- download .gz raw data file locally (if doesn't exist)
- decompress file (if .txt version doesn't exist)
- iterate over file to build up a dictionary of domain -> (article, pageview count)
- go through those grouped results, sort each domain's list, and take top 25
- write results to file
- return file path to results


# future considerations

## memory

quick benchmark data point for one hour:

- 317MB for date=2019-10-01, hour=4, starting from having to download raw file
- 64KB for same date/hour, with results cached

other:

- blacklist items can grow, so a huge set could be costly. one idea is to cache articles per domain into files, and only read these files if we encounter the domain. storing "domain:article" keys in another cache system (like distributed redis) would also help because we can check multiple keys in bulk (e.g. send over 100 at a time) and not have to store these blacklist items in memory


## runtime

quick data points:

- ~30 seconds for one day, nothing cached
- ~7 seconds for computation from decompressed results

other:

- instead of collecting DOMAIN -> [list of articles], I would look to do DOMAIN -> [collections.dequeue maxsize of 25]. this would help reduce maintaining all the articles per domain in memory like I'm doing now, and since I will sort as I go, we don't need a separate sorting opration.
- each hour can be run in parallel, so either introduce async principles or dispatch work to a queue (e.g. redis, celery, RabbitMQ) and poll for results in some shared storage system (e.g. database, file server)

# addressing questions

### infrastructure

- add Dockerfile for standardization
- trigger unit and integration testing after docker image build
- metrics pushed to DataDog :) (such as API download failures, computation failures)

### system design
- make concept of results server; this dispatches work and serves results
- make concept of a worker; smallest unit of work is a date/hour/domain that can be independently computed
- distribute work via queueing
- introduce shared storage system
- keep compressed/decompressed data sets for some time in case of failures (e.g. computation logic has changed, so we can rerun on older data sets)
- every hour, trigger a job to compute the latest date/hour so that it's cached for future requests
- we'd only need a few workers to keep up with daily scheduled processing, but for giant backfills we'd ramp up workers
- for reprocessing, have an override flag for recomputing

general steps for data pipeline worker with example inputs 2019-10-01/15:00/en (date/hour/domain):
1. check if results available in database/cache. we're done if we have them
2. set sentinel value saying "we're working on this date/hour/domain right now"
3. download domain data from internal storage
4. (download decompressed data from internal storage, split into domains, save to storage)
5. (download raw data from API or internal storage, decompress, and do (3) and (4))
6. go through the domain's articles and pageviews to get top 25
7. save to results in database/cache

results server:
- if results are in, serve them
- if no results, schedule job via queue and return an in-progress code (this would mean the client needs to poll for results)
