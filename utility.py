from datetime import datetime, timedelta
import gzip
import os
from pathlib import Path

import requests


def each_hour_in_range(from_datetime, to_datetime):
    from_hour = datetime(from_datetime.year,
                         from_datetime.month,
                         from_datetime.day,
                         from_datetime.hour)
    current = from_hour
    while current <= to_datetime:
        yield current
        current += timedelta(hours=1)


def extract_datetime_from_filename(filename):
    _, filename_chunk = filename.split('-', 1)
    date_string = filename_chunk[:11]
    return datetime.strptime(date_string, '%Y%m%d-%H')


def get_local_file_path(filename):
    file_datetime = extract_datetime_from_filename(filename)
    year = file_datetime.year
    month = file_datetime.month
    day = file_datetime.day
    return Path('.') / str(year) / str(month) / str(day) / filename


def decompress_file_and_write(filepath_compressed):
    """
    filepath is Path object of the gzip-compressed file
    """
    filename = filepath_compressed.parts[-1]
    compressed_file = gzip.GzipFile(filename=filepath_compressed)
    filepath = get_local_file_path(filename.replace('.gz', '.txt'))
    if not filepath.exists():
        with open(filepath, 'wb') as f:
            f.write(compressed_file.read())

    return filepath


def each_line_of_filepath(filepath):
    with filepath.open() as f:
        line = f.readline().rstrip('\n')
        while line:
            yield line
            line = f.readline().rstrip('\n')


def get_and_write_file(url, to_filepath=None):
    response = requests.get(url, stream=True)
    if not response.ok:
        # TODO: should we automatically retry a couple times?
        return None

    if to_filepath is None:
        filename = response.url.rsplit('/', 1)[-1]
        to_filepath = get_local_file_path(filename)

    if not to_filepath.exists():
        os.makedirs(to_filepath.parent, exist_ok=True)

        with open(to_filepath, 'wb') as f:
            for chunk in response:
                f.write(chunk)

    if to_filepath.suffix == '.gz':
        to_filepath = decompress_file_and_write(to_filepath)

    return to_filepath
